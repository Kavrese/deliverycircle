package com.example.deliverycircletest.reg_screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.deliverycircletest.R
import com.example.deliverycircletest.auth_screen.AuthActivity
import com.example.deliverycircletest.common.Account
import com.example.deliverycircletest.common.OnResults
import com.example.deliverycircletest.main_screen.MainActivity
import com.example.deliverycircletest.reg
import com.example.deliverycircletest.setDataUser
import com.example.deliverycircletest.showMessage
import kotlinx.android.synthetic.main.activity_reg.*
import retrofit2.Response

class RegActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg)

        back_to_auth.setOnClickListener {
            startActivity(Intent(this, AuthActivity::class.java))
            finish()
        }

        reg_reg.setOnClickListener {
            val textEmail = email_reg.text.toString()
            val textFirstName = name_reg.text.toString()
            val textLastName = last_reg.text.toString()
            val textPassword = password_reg.text.toString()
            val textPassword2 = password2_reg.text.toString()
            if (textEmail.isNotEmpty() && textPassword.isNotEmpty() && textPassword.isNotEmpty() && textFirstName.isNotEmpty() && textLastName.isNotEmpty()){
                if (textPassword == textPassword2) {
                    if (textPassword.length >= 6) {
                        reg(
                            this,
                            Account(textFirstName, textLastName, textPassword, textEmail),
                            object :
                                OnResults {
                                override fun onResponse(response: Response<Account>) {
                                    setDataUser(this@RegActivity, response.body()!!)
                                    startActivity(
                                        Intent(
                                            this@RegActivity,
                                            MainActivity::class.java
                                        )
                                    )
                                    finish()
                                }
                            })
                    }else{
                        showMessage(this, "Password minimal length 6", "Try again")
                    }
                }else{
                    showMessage(this, "Passwords not equals", "Try again")
                }
            }else{
                showMessage(this, "Empty text", "Try again")
            }
        }
    }
}