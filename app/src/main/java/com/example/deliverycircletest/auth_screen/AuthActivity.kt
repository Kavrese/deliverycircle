package com.example.deliverycircletest.auth_screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.deliverycircletest.R
import com.example.deliverycircletest.reg_screen.RegActivity
import com.example.deliverycircletest.auth
import com.example.deliverycircletest.common.Account
import com.example.deliverycircletest.common.OnResults
import com.example.deliverycircletest.main_screen.MainActivity
import com.example.deliverycircletest.setDataUser
import com.example.deliverycircletest.showMessage
import kotlinx.android.synthetic.main.activity_auth.*
import retrofit2.Response

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        reg_auth.setOnClickListener {
            startActivity(Intent(this, RegActivity::class.java))
            finish()
        }

        auth_auth.setOnClickListener {
            val textEmail = email_auth.text.toString()
            val textPassword = password_auth.text.toString()
            if (textEmail.isNotEmpty() && textPassword.isNotEmpty()){
                auth(this, Account(null, null, textPassword, textEmail), object: OnResults {
                    override fun onResponse(response: Response<Account>) {
                        setDataUser(this@AuthActivity, response.body()!!)
                        startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                        finish()
                    }
                })
            }else{
                showMessage(this, "Empty text", "Try again")
            }
        }
    }
}