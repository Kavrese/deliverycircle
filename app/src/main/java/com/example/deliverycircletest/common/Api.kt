package com.example.deliverycircletest.common

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface Api {
    @PUT("accounts/{id}.json")
    fun createAccount(@Path("id") id: String, @Body body: Account): Call<Account>

    @GET("accounts/{id}.json")
    fun getAccount(@Path("id") id: String): Call<Account>

    @GET("categories.json")
    fun getCategories(): Call<List<Category>>
}