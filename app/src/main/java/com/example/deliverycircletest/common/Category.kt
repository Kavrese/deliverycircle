package com.example.deliverycircletest.common

data class Category(
    val name: String,
    val path: String
)
