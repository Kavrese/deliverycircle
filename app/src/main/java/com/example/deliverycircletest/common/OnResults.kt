package com.example.deliverycircletest.common

import com.example.deliverycircletest.common.Account
import retrofit2.Response

interface OnResults {
    fun onResponse(response: Response<Account>){}
}