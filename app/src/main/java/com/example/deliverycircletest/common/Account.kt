package com.example.deliverycircletest.common

data class Account(
    val firstname: String?,
    val lastname: String?,
    val password: String,
    val email: String
)