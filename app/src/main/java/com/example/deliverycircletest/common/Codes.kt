package com.example.deliverycircletest

import android.app.AlertDialog
import android.content.Context
import com.example.deliverycircletest.common.Account
import com.example.deliverycircletest.common.Api
import com.example.deliverycircletest.common.Info
import com.example.deliverycircletest.common.OnResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun initRetrofit(): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://tasksworldskills-default-rtdb.firebaseio.com/Delivery%D0%A1ircle/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

fun showMessage(context: Context, title: String, message: String){
    val dialog = AlertDialog.Builder(context)
        .setNegativeButton("OK"
        ) { dialog, which -> }
        .setTitle(title)
        .setMessage(message)
    dialog.show()
}

fun reg(context: Context, account: Account, onResults: OnResults){
    initRetrofit().create(Api::class.java).createAccount(account.email.substringBefore("@"), account).enqueue(object: Callback<Account>{
        override fun onResponse(call: Call<Account>, response: Response<Account>) {
            if (response.body() != null){
                onResults.onResponse(response)
            }else{
                showMessage(context, "Fail Create Account", "Try again")
            }
        }

        override fun onFailure(call: Call<Account>, t: Throwable) {
            showMessage(context, "Fail Create Account", "${t.message!!}\nTry again")
        }
    })
}

fun auth(context: Context, account: Account, onResults: OnResults){
    initRetrofit().create(Api::class.java).getAccount(account.email.substringBefore("@")).enqueue(object: Callback<Account>{
        override fun onResponse(call: Call<Account>, response: Response<Account>) {
            if (response.body() != null){
                val model = response.body()!!
                if (model.email == account.email && model.password == account.password){
                    onResults.onResponse(response)
                }else{
                    showMessage(context, "Account not found", "Try again")
                }
            }else{
                showMessage(context, "Fail Auth Account", "Try again")
            }
        }

        override fun onFailure(call: Call<Account>, t: Throwable) {
            showMessage(context, "Fail Auth Account", "Try again")
        }
    })
}

fun setDataUser(context: Context, nowAccount: Account){
    val sh = context.getSharedPreferences("0", 0)
    sh.edit()
        .putBoolean("first", false)
        .putString("password", nowAccount.password)
        .putString("firstname", nowAccount.firstname)
        .putString("lastname", nowAccount.lastname)
        .putString("email", nowAccount.email)
        .apply()
}