package com.example.deliverycircletest.main_screen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.deliverycircletest.R
import com.example.deliverycircletest.common.Category

class AdapterRecyclerView(private var listData: List<Category>): RecyclerView.Adapter<AdapterRecyclerView.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.name)
        val img = itemView.findViewById<ImageView>(R.id.img_category)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_view, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = listData[position].name
        Glide.with(holder.itemView.context)
            .load(listData[position].path)
            .into(holder.img)
    }

    override fun getItemCount(): Int = listData.size

    fun setData(listNewData: List<Category>){
        listData = listNewData
        notifyDataSetChanged()
    }
}