package com.example.deliverycircletest.main_screen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.deliverycircletest.R
import com.example.deliverycircletest.common.Api
import com.example.deliverycircletest.common.Category
import com.example.deliverycircletest.common.Info
import com.example.deliverycircletest.initRetrofit
import com.example.deliverycircletest.showMessage
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rec.apply {
            adapter = AdapterRecyclerView(arrayListOf())
            layoutManager = GridLayoutManager(this@MainActivity, 3, GridLayoutManager.HORIZONTAL, false)
        }
        getCategories()
    }

    private fun getCategories(){
        initRetrofit().create(Api::class.java).getCategories().enqueue(object: Callback<List<Category>>{
            override fun onResponse(
                call: Call<List<Category>>,
                response: Response<List<Category>>
            ) {
                if (response.body() != null){
                    (rec.adapter as AdapterRecyclerView).setData(response.body()!!)
                }else{
                    showMessage(this@MainActivity, "Fail get categories", "Try again")
                }
            }

            override fun onFailure(call: Call<List<Category>>, t: Throwable) {
                showMessage(this@MainActivity, "Fail get categories", "Try again")
            }

        })
    }


}