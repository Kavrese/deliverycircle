package com.example.deliverycircletest.splash_screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.deliverycircletest.R
import com.example.deliverycircletest.common.Account
import com.example.deliverycircletest.common.Info
import com.example.deliverycircletest.main_screen.MainActivity
import com.example.deliverycircletest.view_pager_screen.ViewPagerActivity

class SplashActivity : AppCompatActivity() {
    private lateinit var intent_: Intent
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        intent_ = Intent(this@SplashActivity, ViewPagerActivity::class.java)
        getDataUser()

        Handler().postDelayed({
            startActivity(intent_)
            finish()
        }, 1000)
    }

    private fun getDataUser(){
        val sh = getSharedPreferences("0", 0)
        val first = sh.getBoolean("first", true)
        if (!first) {
            val email = sh.getString("email", null)
            val firstname = sh.getString("firstname", null)
            val lastname = sh.getString("lastname", null)
            val password = sh.getString("password", null)
            Info.userInfo = Account(firstname, lastname, password!!, email!!)
            intent_ = Intent(this, MainActivity::class.java)
        }
    }
}