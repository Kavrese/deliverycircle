package com.example.deliverycircletest.view_pager_screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.content.res.AppCompatResources
import androidx.viewpager2.widget.ViewPager2
import com.example.deliverycircletest.R
import com.example.deliverycircletest.auth_screen.AuthActivity
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_view_pager.*

class ViewPagerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_pager)

        pager.adapter = AdapterViewPager(arrayListOf(
            AppCompatResources.getDrawable(this, R.drawable.ic_screen1)!!,
            AppCompatResources.getDrawable(this, R.drawable.ic_screen2)!!,
        ))

        TabLayoutMediator(tab, pager){ tab, _ ->
            pager.setCurrentItem(tab.position, true)
        }.attach()

        button.setOnClickListener {
            startActivity(Intent(this@ViewPagerActivity, AuthActivity::class.java))
            finish()
        }

        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position == 0){
                    button.text = "SKIP"
                }else{
                    button.text = "NEXT"
                }
            }
        })
    }
}